module OneCM
  class MediaGroupBlock < Liquid::Block
    def initialize(tag_name, tab, tokens)
        super
    end
    def render(context)
      content = super.strip
      return "<section class=\"media-group\">#{content}</section>"
    end
  end
end

Liquid::Template.register_tag("mediagroup", OneCM::MediaGroupBlock)
