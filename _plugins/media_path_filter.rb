module OneCM
  module MediaPathFilter
    def media_path(input, width=0)
      ext = File.extname(input)
      
      base = "#{File.dirname(input)}/#{File.basename(input, ext)}"
      media_base = '/media/'

      if width > 0 && ext != '.gif'
        base += "_#{width}w"
        media_base += 'resized/'
      end

      return "#{media_base}#{base}#{ext}"
    end
  end
end

Liquid::Template.register_filter(OneCM::MediaPathFilter)
