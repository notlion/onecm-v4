---
layout: project
title: Written Images
date: 2011-11-07
categories: projects
---

_Tangled_ is a generative artwork created for the [Written Images](http://writtenimages.net/) book project. A new artwork is generated uniquely for each of the ~230 copies of the book. The software generates a randomized network, applies iterative spring physics, and generates a shell mesh using the technique described in the paper [Solidifying Wireframes](http://www.viz.tamu.edu/faculty/ergun/research/topology/papers/caadfutures05a.pdf).  

In addition to the book, some renders and 3d prints of the generated forms were produced.

Built in C++ with [Cinder](https://libcinder.org/).

{% comment %}
{% include media_image.html path="tangled/5247776003.png" %}
{% include media_image.html path="tangled/4876806767.png" %}
{% include media_image.html path="tangled/7138176249.jpg" %}
{% mediagroup %}
{% include media_image.html path="tangled/5246196318.png" %}
{% endmediagroup %}
{% include media_image.html path="tangled/4796536162.png" %}
{% include media_image.html path="tangled/4786310336.png" %}
{% include media_image.html path="tangled/4785678619.png" %}
{% endcomment %}

{% mediagroup %}
{% include media_image.html path="tangled/6986450744.jpg" %}
{% include media_image.html path="tangled/6986449724.jpg" %}
{% include media_image.html path="tangled/5233537756.gif" %}
{% endmediagroup %}

{% include media_image.html path="tangled/4877416832.png" %}

{% mediagroup %}
{% include media_image.html path="tangled/4795906185.png" %}
{% include media_image.html path="tangled/4795906703.png" %}
{% include media_image.html path="tangled/4793286170.png" %}
{% include media_image.html path="tangled/4795906359.png" %}
{% include media_image.html path="tangled/4796535794.png" %}
{% include media_image.html path="tangled/4796536362.png" %}
{% include media_image.html path="tangled/4796536302.png" %}
{% include media_image.html path="tangled/4796536212.png" %}
{% include media_image.html path="tangled/4796536066.png" %}
{% include media_image.html path="tangled/4796535992.png" %}
{% include media_image.html path="tangled/4795906539.png" %}
{% include media_image.html path="tangled/4795906291.png" %}
{% include media_image.html path="tangled/4793286140.png" %}
{% include media_image.html path="tangled/4786310260.png" %}
{% include media_image.html path="tangled/4785678525.png" %}
{% endmediagroup %}

{% mediagroup %}
{% include media_image.html path="tangled/5228063930.gif" %}
{% include media_image.html path="tangled/5225562041.png" %}
{% include media_image.html path="tangled/5226158698.png" %}
{% include media_image.html path="tangled/5226158682.png" %}
{% include media_image.html path="tangled/5226136412.png" %}
{% include media_image.html path="tangled/5226136380.png" %}
{% include media_image.html path="tangled/5225562083.png" %}
{% include media_image.html path="tangled/5225539633.png" %}
{% endmediagroup %}
