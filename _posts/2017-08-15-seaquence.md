---
layout: project
title: Seaquence
date: 2017-08-15
link_url: https://oknk.studio/seaquence/
categories: projects
---

_"A beautiful and playful multi-voice synthesizer"_

Seaquence was reborn with an interface designed for multitouch and a completely new high quality synthesis engine. The iOS version was released March 2017 after more than 2 years of development.

When we created the multitouch version one goal was for the app to become more of a performance instrument. The interface is more ergonomic, the synth is beefed up -- each voice has a filter effect, and we support MIDI I/O for driving other gear with the internal sequencer, or triggering the internal synth externally. 

Seaquence is a product of [Okaynokay](https://oknk.studio/).

{% mediagroup %}
{% include media_image.html path="seaquence/screenshot_iPad12.9_2x_0.png" %}
{% include media_image.html path="seaquence/screenshot_iPhone6p_2.3x_1.png" %}
{% include media_image.html path="seaquence/screenshot_iPhone6p_2.3x_2.png" %}
{% include media_image.html path="seaquence/screenshot_iPhone6p_2.3x_3.png" %}
{% include media_image.html path="seaquence/screenshot_iPhone6p_2.3x_4.png" %}
{% endmediagroup %}

{% include media_image.html path="seaquence/seaq_logo_sticker_1024.png" %}

{% comment %}
{% include media_image.html path="seaquence/screenshot_iPad12.9_2x_1.png" %}
{% include media_image.html path="seaquence/screenshot_iPad12.9_2x_2.png" %}
{% include media_image.html path="seaquence/screenshot_iPad12.9_2x_3.png" %}
{% include media_image.html path="seaquence/screenshot_iPad12.9_2x_4.png" %}
{% include media_image.html path="seaquence/screenshot_iPhone6p_2.3x_0.png" %}
{% endcomment %}
