---
layout: project
title: Dynamic Sidewalk
categories: projects
---

A prototype interative sidewalk installation developed with [Smoothstep](http://smoothstep.net) at [Dynamicland](https://dynamicland.org) in Oakland, California.

LEDs are installed underneath glass panels embedded in the sidewalk. Two infrared cameras scan the area from above and trigger a ripple effect when the panels are occluded.

{% include video.html youtube="AsoQK87HkrQ" %}
