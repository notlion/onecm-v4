---
layout: project
title: Branching Studies
thumbnail: branching/branching_stars_0.png
categories: works featured
---

These studies were grown with a self-avoiding generative system. Original files are SVG but have been converted to PNG here in order to reduce filesize.

{% include media_image.html path="branching/branching_stars_0.png" %}
{% include media_image.html path="branching/Export_190203_015306.png" %}
{% include media_image.html path="branching/Export_190203_024719.png" %}
{% include media_image.html path="branching/Export_190203_035315.png" %}
{% include media_image.html path="branching/Export_190203_051551.png" %}
{% include media_image.html path="branching/Export_190203_055958.png" %}
{% include media_image.html path="branching/Export_190203_074152.png" %}

{% mediagroup %}
{% include media_image.html path="branching/Export_190203_011051.png" %}
{% include media_image.html path="branching/Export_190203_022848.png" %}
{% include media_image.html path="branching/Export_190202_205547.png" %}
{% endmediagroup %}

{% include media_image.html path="branching/sticker-design-1.png" %}
{% include media_image.html path="branching/sticker-design-0.png" %}

<!-- {% include media_image.html path="branching/branching-0.png" %}
{% include media_image.html path="branching/branching-1.png" %} -->
