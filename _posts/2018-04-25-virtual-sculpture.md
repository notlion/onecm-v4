---
layout: project
title: VR Sculpture
date: 2018-04-25
categories: projects
---

Studies in virtual media exploring embodied mark making. Artworks made using Gravity Sketch and Oculus Medium with the Vive VR system.

{% mediagroup %}
{% include media_image.html path="gravity_sketches/render_5.jpg" %}
{% include media_image.html path="gravity_sketches/launch_render6.jpg" %}
{% include media_image.html path="gravity_sketches/launch_render8.jpg" %}
{% include media_image.html path="gravity_sketches/render_6.jpg" %}
{% include media_image.html path="gravity_sketches/render_7.jpg" %}
{% endmediagroup %}
{% include media_image.html path="gravity_sketches/launch_render2.jpg" %}

{% mediagroup %}
{% include media_image.html path="gravity_sketches/25240693887.jpg" %}
{% include media_image.html path="gravity_sketches/40112939601.jpg" %}
{% include media_image.html path="gravity_sketches/40112940361.jpg" %}
{% endmediagroup %}

{% mediagroup %}
{% include media_image.html path="gravity_sketches/swirl_render_2_hi.png" %}
{% include media_image.html path="gravity_sketches/swirl_render_1_hi.png" %}
{% include media_image.html path="gravity_sketches/swirl_render_4_hi.png" %}
{% include media_image.html path="gravity_sketches/egg_render_4_hi.png" %}
{% include media_image.html path="gravity_sketches/egg_render_3_hi.png" %}
{% endmediagroup %}

{% mediagroup %}
{% include media_image.html path="gravity_sketches/knot_render_1.png" %}
{% include media_image.html path="gravity_sketches/knot_render_2.png" %}
{% include media_image.html path="gravity_sketches/knot_render_4.png" %}
{% endmediagroup %}

{% mediagroup %}
{% include media_image.html path="gravity_sketches/idp_render_13.png" %}
{% include media_image.html path="gravity_sketches/idp_render_12.png" %}
{% include media_image.html path="gravity_sketches/idp_render_14.png" %}
{% endmediagroup %}

{% comment %}
{% include media_image.html path="gravity_sketches/idp_render_10.png" %}
{% include media_image.html path="gravity_sketches/33921751493.jpg" %}
{% endcomment %}
