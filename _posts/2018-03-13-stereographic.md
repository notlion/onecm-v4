---
layout: project
title: Stereographic
date: 2018-03-13
categories: photography
---

Spherical photos projected stereographically.

{% mediagroup %}
{% include media_image.html path="stereographic_photography/DTLA - LA River - 1st St - Metro Rail Yard.jpg" %}
{% include media_image.html path="stereographic_photography/40163015822.jpg" %}
{% include media_image.html path="stereographic_photography/40163031562.jpg" %}
{% endmediagroup %}

{% include media_image.html path="stereographic_photography/Almond Blossom Frost.jpg" %}
{% include media_image.html path="stereographic_photography/Greg's house in Napa after the fire.jpg" %}

{% mediagroup %}
{% include media_image.html path="stereographic_photography/11025627714.jpg" %}
{% include media_image.html path="stereographic_photography/37843799812.jpg" %}
<!-- {% include media_image.html path="stereographic_photography/12042360084.jpg" %} -->
{% include media_image.html path="stereographic_photography/6887511936.jpg" %}
{% endmediagroup %}

{% include media_image.html path="stereographic_photography/9669553152.jpg" %}

{% mediagroup %}
{% include media_image.html path="stereographic_photography/7942274416.jpg" %}
{% include media_image.html path="stereographic_photography/7942270574.jpg" %}
{% include media_image.html path="stereographic_photography/7928521688.jpg" %}
{% include media_image.html path="stereographic_photography/7928523196.jpg" %}
{% include media_image.html path="stereographic_photography/7942366098.jpg" %}
{% endmediagroup %}

{% mediagroup %}
{% include media_image.html path="stereographic_photography/8701615912.jpg" %}
{% include media_image.html path="stereographic_photography/8690518731.jpg" %}
{% include media_image.html path="stereographic_photography/8691178621.jpg" %}
{% include media_image.html path="stereographic_photography/8695399737.jpg" %}
{% include media_image.html path="stereographic_photography/8695405861.jpg" %}
{% endmediagroup %}

{% mediagroup %}
{% include media_image.html path="stereographic_photography/8740348494.jpg" %}
{% include media_image.html path="stereographic_photography/8736910179.jpg" %}
{% include media_image.html path="stereographic_photography/8740320892.jpg" %}
{% endmediagroup %}

{% include media_image.html path="stereographic_photography/9506683596.jpg" %}
{% include media_image.html path="stereographic_photography/9506692666.jpg" %}

{% comment %}
{% include media_image.html path="stereographic_photography/8691634728.jpg" %}
{% include media_image.html path="stereographic_photography/6311813214.jpg" %}
{% include media_image.html path="stereographic_photography/6321044249.jpg" %}
{% include media_image.html path="stereographic_photography/6321568072.jpg" %}
{% include media_image.html path="stereographic_photography/6321657520.jpg" %}
{% include media_image.html path="stereographic_photography/6403816583.jpg" %}
{% include media_image.html path="stereographic_photography/6403818053.jpg" %}
{% include media_image.html path="stereographic_photography/6417697539.jpg" %}
{% include media_image.html path="stereographic_photography/6440902099.jpg" %}
{% include media_image.html path="stereographic_photography/6846222286.jpg" %}
{% include media_image.html path="stereographic_photography/6854293722.jpg" %}
{% include media_image.html path="stereographic_photography/7166969762.jpg" %}
{% include media_image.html path="stereographic_photography/7239174304.jpg" %}
{% include media_image.html path="stereographic_photography/8763710646.jpg" %}
{% include media_image.html path="stereographic_photography/11611027045.jpg" %}
{% endcomment %}
