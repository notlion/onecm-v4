---
layout: project
title: Kuler Dataset Explorations
date: 2007-01-01
categories: projects
---

Work for [Stamen Design](https://stamen.com/) and Adobe [Kuler](https://color.adobe.com/explore).

# Slicer

A 3-dimensional browser and data research tool; Slicer allows inspection of user submitted color palettes in the time, hue, saturation and brighness dimensions. Search breadth can also be attenuated such that more or less palettes are shown for a given time.

{% include video.html vimeo="12315606" width=16 height=10 %}

{% mediagroup %}
{% include media_image.html path="stamen_kuler/Kuler Slicer.png" %}
{% include media_image.html path="stamen_kuler/3832901118_d08c56a69d.jpg" %}
{% include media_image.html path="stamen_kuler/3832106441_79bb95aa15.jpg" %}
{% include media_image.html path="stamen_kuler/3832106491_2ea9ffd4b9.jpg" %}
{% include media_image.html path="stamen_kuler/3832105907_8b5c85da2e.jpg" %}
{% include media_image.html path="stamen_kuler/3832903026_95e2dfc40c.jpg" %}
{% endmediagroup %}

# Infographic Designs

Static visualizations of the colors used. binned by "base" color, top countries, and seasons. These were output as PDFs with the intent to produce posters or other printed material.

{% mediagroup %}
{% include media_image.html path="stamen_kuler/Kulers Fermat 2007 BaseColor.png" %}
{% include media_image.html path="stamen_kuler/Kulers Fermat 2007 WholeWorld.png" %}
{% include media_image.html path="stamen_kuler/Kulers Fermat 2007 Top4 Countries.png" %}
{% include media_image.html path="stamen_kuler/Kulers Fermat 2007 Top16 Countries.png" %}
{% include media_image.html path="stamen_kuler/Kulers World 2007 Top9 Countries.png" %}
{% include media_image.html path="stamen_kuler/Kulers World 2007 Top100 Countries.png" %}
{% include media_image.html path="stamen_kuler/Kulers Fermat 2007 Seasons.png" %}
{% endmediagroup %}
