---
layout: project
title: Tilt Brush Sketches
date: 2018-04-25
categories: projects
---

Experimental immersive artworks made with the VR painting program Tilt Brush.

{% mediagroup %}
{% include media_image.html path="gravity_sketches/33921760363.jpg" %}
{% include media_image.html path="gravity_sketches/33921758103.jpg" %}
{% include media_image.html path="gravity_sketches/33921759473.jpg" %}
{% include media_image.html path="gravity_sketches/33921761213.jpg" %}
{% include media_image.html path="gravity_sketches/33921762653.jpg" %}
{% endmediagroup %}

{% mediagroup %}
{% include media_image.html path="gravity_sketches/33921750413.jpg" %}
{% include media_image.html path="gravity_sketches/33921757263.jpg" %}
{% include media_image.html path="gravity_sketches/33921752573.jpg" %}
{% include media_image.html path="gravity_sketches/33921753503.jpg" %}
{% include media_image.html path="gravity_sketches/33921754463.jpg" %}
{% include media_image.html path="gravity_sketches/33921755343.jpg" %}
{% include media_image.html path="gravity_sketches/33921756333.jpg" %}
{% endmediagroup %}
