---
layout: project
title: Seaquence Flash
date: 2010-09-29
link_url: http://seaquence.org/
categories: projects
---

The original Seaquence website features a 10-voice 22050Hz sample-based synthesizer using the Pixel Bender shading language for sample generation.

{% include media_image.html path="seaquence/5037549070_4825a0fa03_o.png" %}
