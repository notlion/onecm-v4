---
layout: project
title: Broken Type
date: 2010-01-23
categories: works
---

_"Something is .. broken"_

Typographic studies with fracture simulation. Written in C++ with OpenFrameworks.  
Output to screen and vinyl cutter.

{% include video.html vimeo="8927563" width=640 height=360 %}

{% mediagroup %}
{% endmediagroup %}

{% include media_image.html path="broken_type/4392173886.jpg" %}

{% mediagroup %}
{% include media_image.html path="broken_type/4387981891.png" %}
{% include media_image.html path="broken_type/4388745304.png" %}
{% include media_image.html path="broken_type/4388745100.png" %}
{% endmediagroup %}

{% include media_image.html path="broken_type/4392173986.jpg" %}

{% mediagroup %}
{% include media_image.html path="broken_type/4330163046.png" %}
{% include media_image.html path="broken_type/4329429031.png" %}
{% include media_image.html path="broken_type/4320967259.png" %}
{% include media_image.html path="broken_type/4320967319.png" %}
{% include media_image.html path="broken_type/4320967369.png" %}
{% include media_image.html path="broken_type/4321700634.png" %}
{% include media_image.html path="broken_type/4321700722.png" %}
{% include media_image.html path="broken_type/4329429119.png" %}
{% include media_image.html path="broken_type/4329429157.png" %}
{% include media_image.html path="broken_type/4330162960.png" %}
{% include media_image.html path="broken_type/4330163142.png" %}
{% endmediagroup %}

{% comment %}
{% include media_image.html path="broken_type/4296748889.jpg" %}
{% endcomment %}
