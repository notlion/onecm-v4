---
layout: project
title: Streetview Stereographic
link_url: http://notlion.github.io/streetview-stereographic
date: 2012-02-01
categories: works
---

Google Street View imagery projected stereographically with a built in shader-editor.

As of 2018 Google has discontinued free use of their Maps API and changed the way some of the Street View tiles are stored. Due to this, the live website is now partially defunct.

{% mediagroup %}
{% include media_image.html path="streetview_stereographic/Streetview Stereographic.png" %}
{% include media_image.html path="streetview_stereographic/VEMILEZY.jpg" %}
{% include media_image.html path="streetview_stereographic/RJBZTECH.jpg" %}
{% include media_image.html path="streetview_stereographic/FYFABKGN.jpg" %}
{% include media_image.html path="streetview_stereographic/KQPHJEKZ.jpg" %}
{% include media_image.html path="streetview_stereographic/189 Wuhu Street, Hung Hom, Kowloon.png" %}
{% include media_image.html path="streetview_stereographic/osaka.png" %}
{% endmediagroup %}

{% mediagroup %}
{% include media_image.html path="streetview_stereographic/chunktification.png" %}
{% include media_image.html path="streetview_stereographic/都道431号線, Shinjuku, Tokyo.png" %}
{% include media_image.html path="streetview_stereographic/1 Ma Chai Hang Road, Wong Tai Sin, Kowloon.png" %}
{% include media_image.html path="streetview_stereographic/hahajima.png" %}
{% include media_image.html path="streetview_stereographic/Pixel Brooklyn.png" %}
{% include media_image.html path="streetview_stereographic/Sanluis Rio Colorado - Magdalena De Kino, Sonora.png" %}
{% endmediagroup %}

{% mediagroup %}
{% include media_image.html path="streetview_stereographic/110grind.gif" %}
{% include media_image.html path="streetview_stereographic/bents.gif" %}
{% include media_image.html path="streetview_stereographic/fur.gif" %}
{% include media_image.html path="streetview_stereographic/jitter.gif" %}
{% include media_image.html path="streetview_stereographic/R544.gif" %}
{% include media_image.html path="streetview_stereographic/SantaRosalia-Loreto.gif" %}
{% include media_image.html path="streetview_stereographic/Screen-Shot-2012-01-02-at-12.08.49-AM.gif" %}
{% include media_image.html path="streetview_stereographic/Strada Barnova, Bucharest, Bucuresti.gif" %}
{% endmediagroup %}
