---
layout: project
title: Chrome Canopy
link_url: http://onecm.com/projects/canopy
date: 2009-03-18
categories: works
---

Chrome Canopy is a novel fractal zoomer built for Google as one of the initial batch of the [Chrome Experiments](https://experiments.withgoogle.com/chrome) JavaScript showcase. It was designed to showcase the improved speed of the Chrome browser's V8 engine, as well as the then-new `<canvas>` tag.

{% include media_image.html path="chrome_canopy/Chrome Canopy.png" %}
