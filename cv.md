---
layout: page
title: Curriculum Vitæ
---

Ryan Alexander is an designer and software engineer currently focused on interactive audio/visual experience design. His recent works include musical playthings, 3d modeling software, generative simulations, live visual performance, spherical photography, and motion graphics experiments.

Ryan was a resident at Gray Area from 2009-2010, where he worked on the original Seaquence [website](http://seaquence.org) and developed techniques for projection-mapped sculpture. He has been featured in the [Written Images](http://www.writtenimages.net) generative book, worked on mapping projects such as [StreetView Stereographic](https://onecm.com/v4/streetview-stereographic.html) and built an online collaborative voxel modeling tool at Otherlab.

In 2017, Ryan released [Seaquence](http://oknk.studio/seaquence/) for iOS under [Okaynokay](https://oknk.studio/), an interactive design studio co-founded with [Gabriel Dunne](http://gabrieldunne.com/). Okaynokay has since produced [Twilight Engines](https://oknk.studio/twilightengines/), a site-specific virtual sculpture by [Vishal K Dar](http://www.vishalkdar.com/) at the [Headlands Center for the Arts](http://www.headlands.org/).

Ryan is currently working on [Cuttle](https://cuttle.xyz), a new hybrid procedural design tool for digital cutting machines.

### Current Residence

San Francisco, California

### Biography

- 2020---Present Working on **Cuttle** [★](https://cuttle.xyz) A procedural design tool for digital cutting machines
- 2019 **Okaynokay** Developed _Twilight Engines_ by Vishal K. Dar, an architectural-scale AR experience for iOS, using ARKit and Unity
- 2017---2019 **Onformative**, Developed performant ambient graphical apps for Samsung using a custom-built minimal C++ / OpenGL ES based framework
- 2015---2016 **Next Thing Co.**, Designed and developed the _OTTO_ Camera OSD interface. Designed _Pocket CHIP_ user interface and physical keyboard layout
- 2014---2016 **Okaynokay** Developed _Seaquence_ iOS synthesizer app
- 2012---2014 **Otherlab**, Lead developer of _Cube Team_, a collaborative web-based voxel modeling app / Designed and developed interfaces for computational CAD tools
- 2012 **Bloom Studio**, Developed and designed the interfaces of _Planetary_ and _Biologic_ data-visualization apps for iOS
- 2009---2012 Freelance interactive software design, data-visualization and visual effects for Motion Theory, Stamen, Google, etc
- 2007---2009 **Stamen Design**, Developed custom interactive data-visualization software for Adobe, Nike, Twitter, Trulia, etc
- 2007 **Moto**, Developed demo software for prototype multi-touch table
- 2007 Co-founded OooShiny meetup and community for artists working with code
- 2004---2005 **Logan**, Designed and animated motion graphics for television and games. Developed custom visual-effects software tools
- 2001---2004 **Motion Theory**, Designed and developed the company website. Designed and animated motion graphics for television and music videos. Developed custom visual-effects software tools
- 2001 Studied art/animation at **Santa Monica College**, Santa Monica, California
- 2000 Studied animation at **Loyola Marymount University**, Los Angeles, California
- 1999 Summer Internship at **Will Vinton Studios**, Portland, Oregon
- 1981 Born in Santa Barbara, California

### Selected Performance

- 2017 Robert Rich, Live Visuals, Chillits [★](https://youtu.be/bQnWB_v_3MA)
- 2016 Earthen Sea, Animation, Chillits
- 2014 CCRMA Modulations, Live Visuals, Stanford University
- 2013 Christina Chatfield, Live Visuals, Gray Area Bunker Sessions, San Francisco [★](https://vimeo.com/62595702)
- 2013 ATOM™, Live Visuals, San Francisco
- 2013 Peter van Hoesen, Live Visuals, Gray Area Bunker Sessions, San Francisco [★](https://vimeo.com/51616769)
- 2012 Urban Prototyping Festival, Live Visuals, San Francisco
- 2012 CCRMA Modulations, Live Visuals, San Francisco
- 2011 Shigeto, Live visuals, Gray Area, San Francisco [★](https://vimeo.com/23197100)

### Residencies / Collaborations

- 2019 Vishal K Dar in collaboration with Okaynokay, Site-specific virtual sculptures, Headlands Center for the Arts [★](https://oknk.studio/twilightengines/)
- 2009---2010 Artist in Residence, Gray Area Foundation for the Arts, San Francisco

### Exhibition / Installation

- 2018 _METAFLUX_, Studio Space, Emeryville
- 2013 _Art by Code_, CODAME, Public Works, San Francisco
- 2012 _PROTOTYPE_, Gray Area, Taylor Street, San Francisco

### Teaching / Talks

- 2017 Fall Cultural Incubator Showcase: Seaquence, Gray Area, San Francisco
- 2015 _Pixels Unleashed_ (Alternate Shader Techniques), Gray Area, San Francisco
- 2014 Weekly Hack-Jam, Dim Sum Labs, Hong Kong
- 2013 _Particle Shader Toy_, Twitter, San Francisco
- 2012 OpenGL in Processing Workshop, Tokyo Polytechnic University, Kanagawa
- 2012 Data Visualization Group, Intuit, Mountain View
- 2011 Data Visualization Group, Trulia, San Francisco

### Selected Projects

- 2009---2018 _Seaquence_, Playful multi-voice synthesizer app, Adobe Flash, then iOS [★](http://oknk.studio/seaquence)
- 2013---2014 _Cube Team_, Novel collaborative voxel modeling app, Web
- 2012 _Street View Stereographic_, Google Street View Explorer and Shader Toy [★](http://notlion.github.io/streetview-stereographic)
- 2009 _Mycelium_, Processing [★]({{ "/projects/mycelium/" | relative_url }})
- 2001 Motion Theory Website, Adobe Flash

### Awards

- 2001 Art Directors Club (ADC 80), Gold in New Media, motiontheory.com
- 2001 Broadcast Designers Association, Gold in Flash Websites, motiontheory.com
- 2001 Society of Publication Designers (SPD 36), Silver in Interactive, motiontheory.com

### Clients / Professional Activity

#### San Francisco Bay Area

- Dynamicland
- Otherlab
- Bloom Studio
- Stamen Design
- Moto
- Google
- Gray Area Foundation

#### Los Angeles

- Logan
- Motion Theory

#### Berlin

- Onformative

### Software Tools / Languages

An inexhaustive list of the main tools that I tend to reach for when building software things.

- C++17
- Objective-C
- JavaScript
- OpenGL (ES / Core)
- WebGL
- WebAssembly
- Pure Data
- Cinder
- Unity
