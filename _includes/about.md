Hi. My name is **Ryan Alexander**. I'm a designer, software engineer and founder of the interactive design studio [Okaynokay](http://oknk.studio). I also occasionally make physical art, perform live visuals, and manage the international [OooShiny](http://shiny.ooo) art technology group. I'm currently based in San Francisco.

- Email: [ryan@oknk.studio](mailto:ryan@oknk.studio)
- Twitter: [@notlion](https://twitter.com/notlion)
- Instagram: [@lioku](https://www.instagram.com/lioku)

{% include home.html %}
