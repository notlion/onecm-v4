const Taggr_tile_offsets = [
  [-1, -1], [0, -1], [1, -1],
  [-1,  0], [0,  0], [1,  0],
  [-1,  1], [0,  1], [1,  1]
];

class Taggr {
  static rand2(min, max) {
    return min + Math.random() * (max - min);
  }
  
  static randInt2(min, max) {  // min-max inclusive
    return Math.round(min + Math.random() * (max - min));
  }
  
  static distance(p1, p2) {
    const xo = p2[0] - p1[0];
    const yo = p2[1] - p1[1];
    return Math.sqrt(xo * xo + yo * yo);
  }

  static distance2(p1, p2) {
    const xo = p2[0] - p1[0];
    const yo = p2[1] - p1[1];
    return xo * xo + yo * yo;
  }

  _drawBSpline(vertices) {
    const gfx = this.gfx;
    const nv = vertices.length;
    if (nv >= 4) {
      let p0 = vertices[0], p1, p2;
      gfx.beginPath();
      gfx.moveTo(p0[0], p0[1]);
      for (let i = 1, n = nv - 1; i <= n; ++i) {
        p0 = vertices[i - 1];
        p1 = vertices[i];
        if (i == n) {  // finish him
          gfx.bezierCurveTo((2 * p0[0] + p1[0]) / 3, (2 * p0[1] + p1[1]) / 3,
                            (2 * p1[0] + p0[0]) / 3, (2 * p1[1] + p0[1]) / 3,
                            p1[0], p1[1]);
        }
        else {
          p2 = vertices[i + 1];
          gfx.bezierCurveTo((2 * p0[0] + p1[0]) / 3, (2 * p0[1] + p1[1]) / 3,
                            (2 * p1[0] + p0[0]) / 3, (2 * p1[1] + p0[1]) / 3,
                            (p0[0] + 4 * p1[0] + p2[0]) / 6, (p0[1] + 4 * p1[1] + p2[1]) / 6);
        }
      }
      gfx.stroke();
      gfx.closePath();
    }
  }
  
  _drawTiled(drawFunc) {
    const gfx = this.gfx;
    const w = this.canvas.width;
    const h = this.canvas.height;
    for (let offset of Taggr_tile_offsets) {
      gfx.save();
      gfx.translate(offset[0] * w, offset[1] * h);
      drawFunc();
      gfx.restore();
    }
  }

  _drawRiceBG() {
    const gfx = this.gfx;
    for (let t = Taggr.randInt2(5, 10); --t >= 0;) {
      const dx = Taggr.rand2(15, 30) * (Math.random() < 0.5 ? -1 : 1);
      const dy = Taggr.rand2(15, 30) * (Math.random() < 0.5 ? -1 : 1);
      const rnd = Taggr.rand2(20, 80);
      const vertices = [];

      let x = Taggr.rand2(0, this.canvas.width);
      let y = Taggr.rand2(0, this.canvas.height);
      for (let i = Taggr.randInt2(4, 7); --i >= 0;) {
        vertices.push([x += dx + Taggr.rand2(-rnd, rnd),
                       y -= dy + Taggr.rand2(-rnd, rnd)]);
      }
      gfx.strokeStyle = '#dddddd';
      gfx.lineWidth = '2';
      gfx.lineCap = 'round';
      
      this._drawTiled(() => this._drawBSpline(vertices));
    }
  }

  _drawPlusBG() {
    const gfx = this.gfx;
    const plusses = [];
    for (let t = Taggr.randInt2(5, 10); --t >= 0;) {
      const plus = {
        pos: [Taggr.rand2(0, this.canvas.width), Taggr.rand2(0, this.canvas.height)],
        rot: Taggr.rand2(0, Math.PI),
        scale: Taggr.rand2(20, 40)
      };
      for (let i = plusses.length; --i >= 0;) {
        if (Taggr.distance(plus.pos, plusses[i].pos) < plus.scale + plusses[i].scale)
          break;
      }
      if (i == -1) {
        plusses.push(plus);
        _drawTiled(() => {
          gfx.save();
          gfx.translate(plus.pos[0], plus.pos[1]);
          gfx.rotate(plus.rot);
          gfx.scale(plus.scale / 3, plus.scale / 3);
          gfx.fillStyle = '#1b61ff';
          gfx.fillRect(-1, -3, 2, 2);
          gfx.fillRect(1, -1, 2, 2);
          gfx.fillRect(-1, 1, 2, 2);
          gfx.fillRect(-3, -1, 2, 2);
          gfx.fillStyle = '#ffff00';
          gfx.fillRect(-1, -1, 2, 2);
          gfx.restore();
        });
      }
    }
  }

  _drawCircles() {
    const verts = [];
    const circles = [{
      pos: [0, this.canvas.height / 2],
      radius: 0,
      clockwise: false
    }];

    const checkCircle = (circle) => {
      for (let i = circles.length; --i >= 0;) {
        const min_dist = circle.radius + circles[i].radius;
        if (Taggr.distance2(circle.pos, circles[i].pos) <= min_dist * min_dist) {
          return false;
        }
      }
      return true;
    };

    const appendCircle = (circle) => {
      const revolutions = Taggr.rand2(2, 8);
      const facets = Taggr.rand2(4, 6);
      const messyness = circle.radius / facets * 1.1;
      const theta_start = Math.random() * Math.PI * 2;
      const radius = circle.radius * 1.25;
      for (let i = 0, n = facets * revolutions; i < n; ++i) {
        var t = theta_start + i / (n - 1) * Math.PI * 2 * revolutions;
        if (!circle.clockwise) {
          t *= -1;
        }
        verts.push([circle.pos[0] + Math.sin(t) * radius + Taggr.rand2(-messyness, messyness),
                    circle.pos[1] + Math.cos(t) * radius + Taggr.rand2(-messyness, messyness)]);
      }
      circles.push(circle);
    };

    const theta_bias = Taggr.rand2(0.1, 0.5) * (Math.random() < 0.5 ? 1 : -1);
    const theta_rnd = Taggr.rand2(0.1, 0.3);
    let theta = 0;
    for (let i = 1000; --i >= 0;) {
      const radius = Math.min(this.canvas.width, this.canvas.height) * Taggr.rand2(0.01, 0.2);
      const last = circles[circles.length - 1];
      theta += theta_bias + Math.random() * Taggr.rand2(-theta_rnd, theta_rnd);
      const circle = {
        pos: [last.pos[0] + Math.sin(theta) * (radius + last.radius),
              last.pos[1] + Math.cos(theta) * (radius + last.radius)],
        radius: radius,
        clockwise: Math.random() < 0.1 ? !last.clockwise : last.clockwise
      };
      if (checkCircle(circle)) {
        appendCircle(circle);
      }
    }

    let min = [Number.MAX_VALUE, Number.MAX_VALUE];
    let max = [Number.MIN_VALUE, Number.MIN_VALUE];
    for (let i = verts.length; --i >= 0;) {
      min[0] = Math.min(min[0], verts[i][0]);
      min[1] = Math.min(min[1], verts[i][1]);
      max[0] = Math.max(max[0], verts[i][0]);
      max[1] = Math.max(max[1], verts[i][1]);
    }
    const center = [(min[0] + max[0]) / 2, (min[1] + max[1]) / 2];
    const size = [max[0] - min[0], max[1] - min[1]];
    const canvas_size = [this.canvas.width, this.canvas.height];
    const canvas_center = [canvas_size[0] / 2, canvas_size[1] / 2];
    const scale = Math.min(canvas_size[0] / size[0], canvas_size[1] / size[1]);

    const verts_centered = verts.map((v) => {
      return [scale * (v[0] - center[0]) + canvas_center[0],
              scale * (v[1] - center[1]) + canvas_center[1]];
    });

    const gfx = this.gfx;

    gfx.strokeStyle = '#dddddd';
    gfx.lineWidth = '2';
    gfx.lineCap = 'round';

    this._drawBSpline(verts_centered);
  }
  
  _applyBackground() {
    const data_url = this.canvas.toDataURL('image/png');
    document.body.style.backgroundImage = `url(${data_url})`;
  }
  
  constructor(canvas) {
    this.canvas = canvas;
    this.gfx = this.canvas.getContext('2d');
  }

  static createFromWidthHeight(width, height) {
    const canvas = document.createElement('canvas');
    canvas.width = width;
    canvas.height = height;
    canvas.style.display = 'none';
    return new Taggr(canvas);
  }
  
  generateBackground() {
    this._drawRiceBG();
    this._applyBackground();
  }
}