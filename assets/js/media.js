const mediaMain = () => {
  const HIGH_RES_IMAGE_WIDTH = 2048;

  const viewer_elem = document.getElementById("media-viewer");
  const viewer_image_elem = document.getElementById("media-viewer-image");
  const viewer_image_container_elem = document.getElementById("media-viewer-image-container");
  const viewer_title_link_elem = document.getElementById("media-viewer-title-link");
  const close_button_elem = document.getElementById("media-viewer-close");
  const image_elems = Array.from(document.querySelectorAll(".media-image>img"));

  const image_src_regex = /(.+_)(\d+)(w\.\w+)$/;

  const makeMediaImageUrlWithWidth = (url, width) => {
    const match = url.match(image_src_regex);
    if (match) {
      return match[1] + width + match[3];
    }
    return url;
  };

  const updateImageTransform = (image) => {
    const xf = mat2d.create();
    
    mat2d.translate(xf, xf, image.center_init);
    mat2d.translate(xf, xf, image.translation);
    mat2d.rotate(xf, xf, image.rotation);
    mat2d.scale(xf, xf, [image.scale, image.scale]);
    mat2d.translate(xf, xf, vec2.negate(vec2.create(), image.center_init));

    mat2d.mul(image.transform, xf, image.transform_init);

    image.element.style.transform = "matrix" + mat2d.str(image.transform).slice(5);
  };

  const resetImageTransform = (image) => {
    mat2d.copy(image.transform_init, image.transform);
    vec2.set(image.translation, 0, 0);
    image.rotation = 0;
    image.scale = 1;
  };

  const clearImageTransform = (image) => {
    mat2d.identity(image.transform);
    resetImageTransform(image);
  };

  const requestImageTransformUpdate = (image) => {
    if (!image.update_requested) {
      image.update_requested = true;
      window.requestAnimationFrame(() => {
        updateImageTransform(image);
        image.update_requested = false;
      });
    }
  };

  const createViewerImage = (element) => {
    const clientToImagePosCentered = (out, image, x, y) => {
      const cx = 0.5 * viewer_elem.clientWidth;
      const cy = 0.5 * viewer_elem.clientHeight;
      const transform_inv = mat2d.invert(mat2d.create(), image.transform);
      return vec2.transformMat2d(out, [x - cx, y - cy], transform_inv);
    };

    const onGestureStart = (image, event) => {
      image.gestures_active += 1;

      const cx = 0.5 * viewer_elem.clientWidth;
      const cy = 0.5 * viewer_elem.clientHeight;

      vec2.set(image.center_init, event.center.x - cx, event.center.y - cy);

      resetImageTransform(image);
    };

    const onGestureEnd = (image, event) => {
      image.gestures_active -= 1;

      if (image.gestures_active === 0) {
        resetImageTransform(image);
      }
    };

    const onPan = (image, event) => {
      event.preventDefault();

      const cx = 0.5 * viewer_elem.clientWidth;
      const cy = 0.5 * viewer_elem.clientHeight;

      vec2.set(image.translation, (event.center.x - cx) - image.center_init[0], (event.center.y - cy) - image.center_init[1]);
      
      requestImageTransformUpdate(image);
  
      return false;
    };
  
    const onRotate = (image, event) => {
      event.preventDefault();
  
      const DEG_TO_RAD = Math.PI / 180;

      if (event.type === "rotatestart") {
        image.rotation_init = event.rotation * DEG_TO_RAD;
      }

      image.rotation = event.rotation * DEG_TO_RAD - image.rotation_init;

      requestImageTransformUpdate(image);
    };
  
    const onPinch = (image, event) => {
      event.preventDefault();

      image.scale = event.scale;

      requestImageTransformUpdate(image);
    };

    const onScroll = (image, event) => {
      event.preventDefault();
      event.stopPropagation();
      
      if (image.gestures_active === 0) {
        resetImageTransform(image);

        if (event.deltaY != 0) {
          const cx = 0.5 * viewer_elem.clientWidth;
          const cy = 0.5 * viewer_elem.clientHeight;
    
          vec2.set(image.center_init, event.clientX - cx, event.clientY - cy);

          const delta = event.deltaY * (event.deltaMode === 0 ? 1.0 : 2.5);
          image.scale = 1.0 - delta * 0.01;

          requestImageTransformUpdate(image);
        }
      }
		};
    
    const manager = new Hammer.Manager(element);
    
    manager.add(new Hammer.Pan({ threshold: 0, pointers: 0 }));
    manager.add(new Hammer.Rotate({ threshold: 0 })).recognizeWith(manager.get("pan"));
    manager.add(new Hammer.Pinch({ threshold: 0 })).recognizeWith([manager.get("pan"), manager.get("rotate")]);
    // manager.add(new Hammer.DoubleTap()
    
    const image = {
      element, 
      manager, 
      
      transform: mat2d.create(),
      transform_init: mat2d.create(),

      translation: vec2.create(),
      rotation: 0,
      scale: 1,

      rotation_init: 0,
      center_init: vec2.create(),
      
      gestures_active: 0,

      update_requested: false,
    };
    
    manager.on("panstart rotatestart pinchstart", event => onGestureStart(image, event));
    manager.on("panend pancancel rotateend rotatecancel pinchcancel", event => onGestureEnd(image, event));
    manager.on("panstart panmove", event => onPan(image, event));
    manager.on("rotatestart rotatemove", event => onRotate(image, event));
    manager.on("pinchstart pinchmove", event => onPinch(image, event));

    element.addEventListener("wheel", event => onScroll(image, event));

    return image;
  };

  const viewer_image = createViewerImage(viewer_image_elem);

  const openViewer = (image_elem) => {
    const src = image_elem.attributes.src.value;
    const url_high = makeMediaImageUrlWithWidth(src, HIGH_RES_IMAGE_WIDTH);
    viewer_image_elem.src = url_high;
    viewer_image_elem.style.backgroundImage = `url(${src})`;
    viewer_elem.classList.add("open");

    const url_original = image_elem.attributes["data-original"].value;
    viewer_title_link_elem.href = url_original;
    viewer_title_link_elem.textContent = image_elem.attributes.alt.value;

    clearImageTransform(viewer_image);
    requestImageTransformUpdate(viewer_image);
  };
  const closeViewer = () => {
    viewer_elem.classList.remove("open");
    viewer_image_elem.src = "";
  };

  const onContainerClick = (event) => {
    if (event.target === viewer_image_container_elem) {
      closeViewer();
    }
  };
  
  close_button_elem.addEventListener("click", event => closeViewer());
  viewer_elem.addEventListener("wheel", event => event.preventDefault());
  viewer_image_container_elem.addEventListener("click", onContainerClick);
  viewer_image_elem.addEventListener("mousedown", event => event.preventDefault());

  const onMediaImageClick = (event) => {
    event.preventDefault();
    openViewer(event.target);
  };

  for (let elem of image_elems) {
    elem.addEventListener("click", onMediaImageClick);
  }
};

window.addEventListener("DOMContentLoaded", event => mediaMain());
