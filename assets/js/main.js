const main = () => {
  const taggr = Taggr.createFromWidthHeight(1024, 1024);
  taggr.generateBackground();
};

window.addEventListener("DOMContentLoaded", event => main());
