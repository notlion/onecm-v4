const home_main = () => {
  const canvas = document.getElementById("home-canvas");
  canvas.width = 720;
  canvas.height = 300;
  const taggr = new Taggr(canvas);
  taggr._drawCircles();
};

window.addEventListener("DOMContentLoaded", event => home_main());
